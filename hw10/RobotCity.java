//Jordan Weintraub
//CSE2 hw10
// 4/20/18
//Objective is to design a Robo ciy using 2 dimmentional arrays 
import java.util.Random;
import java.lang.Math;
import java.util.Scanner;


public class RobotCity{
  public static void main(String[] args){
    for(int i = 0; i < 5; i++){ //loops thru 5 times
      int[][] city = buildCity(); //creates city
      System.out.println("Build City: ");
      display(city);
      Random random = new Random();
      int invaders = random.nextInt(city.length * city[0].length);
      city = invade(city, invaders); //changes city after invasion
      System.out.println("After invasion: ");
      display(city);
      city = update(city); // changes city after update
      System.out.println("After update: ");
      display(city); //prints out city
    }
    
  }
  //builds the city grid
  public static int[][] buildCity(){
    Random dimensions = new Random();
    int northsouth = dimensions.nextInt(5)+10; // sets demensions
    int eastwest = dimensions.nextInt(5)+10;
    int[][] city = new int[northsouth][eastwest];
    for (int i = 0;i < northsouth; i++){
      for (int j = 0; j < eastwest;j++){
        int population = dimensions.nextInt(900)+100; // random population
        city[i][j] = population; //places population on city grid
      }
    }
    return city;
  }
  
  //prints out city grid 
  public static void display(int[][] city){
    for (int i = 0;i < city.length; i++){
      for (int j = 0; j < city[i].length;j++){
          System.out.printf("%-5d", city[i][j]); //allows 5 spaces with left alignment
      }
      System.out.println();
    }
    System.out.println();
  }
  
  //invades city 
  public static int[][] invade(int[][] city, int k){
    Random random = new Random();
    for (int i = 0; i < k; i++){
      int ycorr = random.nextInt(city.length); //random corrdinates
      int xcorr = random.nextInt(city[0].length);
      if (city[ycorr][xcorr] < 0){ //ensures no duplicates
        k++;
      }
      else{
        city[ycorr][xcorr] *= -1;// places invaders
      }
    }
    return city;
  }
  
  //shifts invaders over 1 block
  public static int[][] update(int[][] city){
    for (int i = 0;i < city.length; i++){
      for (int j = 0; j < city[i].length;j++){
        if (city[i][j] < 0){ //checks for invader
          city[i][j] *= -1;
          if (j >= city[i].length -1){ //checks to if next spot is inbounds
            city[i][j] *= -1;
            j+=2;
          }
        }
      } 
       
    }
    return city;
  }
  
}
