//Jordan Weintraub
//CSE 2 hw08 program 2
//04/06/18
// objective is to write three different metohods to give me practice with arrays and searching single dimentional rays

//copied code given from homework prompt
import java.util.Scanner;
import java.util.Random;

public class RemoveElements{
  public static void main(String [] arg){
Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
String answer="";
do{
  System.out.print("Random input 10 ints [0-9]");
  num = randomInput();
  String out = "The original array is:";
  out += listArray(num);
  System.out.println(out);
 
  System.out.print("Enter the index ");
  index = scan.nextInt();
  newArray1 = delete(num,index);
  String out1="The output array is ";
  out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  System.out.println(out1);
 
      System.out.print("Enter the target value ");
  target = scan.nextInt();
  newArray2 = remove(num,target);
  String out2="The output array is ";
  out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  System.out.println(out2);
   
  System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  answer=scan.next();
}while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
String out="{";
for(int j=0;j<num.length;j++){
  if(j>0){
    out+=", ";
  }
  out+=num[j];
}
out+="} ";
return out;
  }
  
  public static int[] randomInput()
  {
      Random rand = new Random();
      int[] array = new int[10]; //creates an array of size 10
      for (int i=0; i<10; i++) //for loop to create 9 random numbers and add them to array
      {
          array[i] = rand.nextInt(10);
      }
      return array;
  }
  
  public static int[] delete(int[] list, int pos)
  {
      int[] array = new int[list.length-1]; //creates an array of size 1 less
      int index = 0;
      if (pos < 0 || pos > list.length-1) //check for invalid input
      {
          System.out.println("Invalid position input!");
          return array;
      }
      for (int i=0; i<list.length; i++)
      {
          if (i == pos) //if i == pos skip over that element
          {
              if (i == 9) //if it is the last element in the array return
              {
                  return array;
              }
              i++;
          }
          array[index] = list[i]; //add to the array
          index++;
      }
      return array;
  }
  
  public static int[] remove(int[] list, int target)
  {
      int size = 0;
      for (int i=0; i<list.length; i++) //goes through the array to check what size it is going to be
      {
          if (list[i] != target)
          {
              size++;
          }
      }
      int[] array = new int[size]; //create an array with a new size
      int index = 0;
      for (int i=0; i<list.length; i++)
      {
          if (list[i] != target) //add it to the new array if it doesnt equal the input value
          {
              array[index] = list[i];
              index++;
          }
      }
      return array;
  }
  
}