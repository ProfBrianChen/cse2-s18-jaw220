//Jordan Weintraub
//CSE 2 hw08 program 1
//04/06/18
//objective is to practice with arrays and searching single dimentional rays
// specfically, prompting grades then finding them through a binary search 
import java.util.Scanner;
import java.util.Random;

public class CSE2Linear{
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in);
    
    int[] grades = new int[15];
    int val; //declaring value
    
    System.out.println("Enter 15 integers for students final grades: ");
    for (int i=0; i<15; i++) //for loop to take in 15 nums
    {
      while (!myScanner.hasNextInt()) //check to see if input is an int
      {
        System.out.println("Error: Please Enter An Int: ");
        myScanner.next();
      }
      val = myScanner.nextInt();
      while (val < 0 || val > 100) //check to see if input is betweeen 0-100
      {
          System.out.println("Error: Please Enter an Int Between 0-100: ");
          while(!myScanner.hasNextInt()) //if not check again
          {
            System.out.println("Error: Please Enter An Int: ");
            myScanner.next(); 
          }
        val = myScanner.nextInt();
      }
      if (i == 0) //if its the first element add it
      {
        grades[i] = val;
      }
      else
      {
        if (val < grades[i-1]) //if not first element check to see if it is larger than previous
        {
          System.out.println("Error: Please enter an int greater than the previous: ");
          while (!myScanner.hasNextInt()) // if fails redo checks for different nums
          {
            System.out.println("Error: Please Enter An Int: ");
            myScanner.next();
          }
          val = myScanner.nextInt();
          while (val < 0 || val > 100)
          {
            System.out.println("Error: Please Enter an Int Between 0-100: ");
            while(!myScanner.hasNextInt())
            {
              System.out.println("Error: Please Enter An Int: ");
              myScanner.next(); 
            }
            val = myScanner.nextInt();
          }
          grades[i] = val;
        }
        else
        {
          grades[i] = val;
        }
      }
    }
    
    System.out.println("The array you entered was: ");
    for (int i=0; i<15; i++) //print out the array the user entered
    {
      System.out.print(grades[i] + " ");
    }
    System.out.println("");
    
    System.out.println("Please enter a grade to search for: ");
    int search = myScanner.nextInt(); //user enters a grade
    
    System.out.println("Found: "+BinarySearch(grades, search)); //use binary search and return if found
    
    grades = scramble(grades); //scramble the grades using the scramble method
    System.out.println("The scrambled array is: ");
    for (int i=0; i<15; i++) //for loop to print out the scrambled array
    {
      System.out.print(grades[i] + " ");
    }
    System.out.println("");
    
    System.out.println("Please enter a grade to search for: ");
    search = myScanner.nextInt(); //user enters grade to search for
    System.out.println("Found: "+LinearSearch(grades, search)); //run linear search and return true if found
    
  }
  
  public static boolean BinarySearch(int[] grades, int val)
  {
    int size = 15;
    int count = 1;
    int index = size/2;
    while (size >= 1)
    {
      System.out.println("Iteration: "+count);
      //System.out.println("Index: "+index);
      //System.out.println("Size: "+size);
      if (grades[index] == val) //if the current index == val return
      {
        return true;
      }
      else if (val < grades[index]) //if val is less then check the left half array using new params
      {
        size = size/2;
        index = size/2;
      }
      else //if val is greater then check the right half using new params
      {
        size = size/2;
        index = index + size/2 + 1;
      }
      count++;
    }
    return false; //if never found return false
  }
  
  public static boolean LinearSearch(int[] grades, int val)
  {
    for (int i=0; i<15; i++) //go through each element and check if = val
    {
      if (grades[i] == val)
      {
        return true;
      }
    }
    return false;
  }
  
  public static int[] scramble(int[] grades)
  {
    Random rand = new Random();
    for (int i=0; i<15; i++) //loop through 15 times and assign elements to random positions
    {
      int index = rand.nextInt(15);
      int temp = grades[index];
      grades[index] = grades[i];
      grades[i] = temp;
    }
    return grades;
  }
  
}