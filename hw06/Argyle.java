//Jordan Weintraub
//CSE2 hw06
// 3/15/19
//please give me a break during grading I struggled with this hw and put a lot of time into it.
//objective is to practice with writing while, for, and do-while loops.

import java.util.Scanner;
public class Argyle { //declaring class
  public static void main (String[] args){ //declaring method
    Scanner myScanner = new Scanner(System.in);
   //declaring all my integers 
    int widthWindow = 0;
    int heightWindow = 0;
    int sizeDiamond = 0;
    int widthStripe = 0; 
    char fillDiamond1 = 0;
    char fillDiamond2 = 0;
    char fillStripe = 0;
    
     System.out.println("Enter positive integer for width of window in characters: ");
  
    while (!myScanner.hasNextInt()){ //while loop the incorrect input so user can repeat input until it is correct
      System.out.println("Enter positive integer for width of window in characters: "); //will continute to print out statement until it is correct
      myScanner.next(); //moves onto next scan 
    }
    widthWindow = myScanner.nextInt(); //beyond scope so it will run the variable once user inputs correctly 
 
    System.out.println("Enter positive integer for height of window in characters: ");
    
    while(!myScanner.hasNextInt()){
      System.out.println("Enter positive integer for height of window in characters: ");
      myScanner.next();
    }
    heightWindow = myScanner.nextInt();
    
    System.out.println("Enter positive integer for width of diamond: ");
    
    while(!myScanner.hasNextInt()){
      System.out.println("Enter positive integer for width of diamond: ");
      myScanner.next();
    }
    sizeDiamond = myScanner.nextInt();
    
    //note to self: make sure its less than half of diamond size AND recognized that it is an odd int
     
    System.out.println("Enter positive ODD integer for width of stripes: ");
    
    while(!myScanner.hasNextInt()) {//while loop to repeat input if input was in incorrect form
      System.out.println("Enter positive ODD integer for width of stripes: ");// printing out again the statement to ask user for input
      myScanner.next();
    }
    widthStripe = myScanner.nextInt(); 
    
    if ((widthStripe % 2) == 0 || (widthStripe > (0.5 * sizeDiamond))){
      System.out.println("Enter positive ODD integer for width of stripes: ");
      widthStripe = myScanner.nextInt();
    }//setting variable to scanned input
 
    while ((widthStripe % 2) == 0 || (widthStripe > (0.5 * sizeDiamond))){
      System.out.println("Enter positive ODD integer for width of stripes: ");// printing out again the statement to ask user for input
      widthStripe = myScanner.nextInt();//move on to next scan
    }
   
    System.out.println("Enter first character to fill diamond: "); //process to get the characters
    String character = myScanner.next();
    fillStripe = character.charAt(0); //gets char as an integer i
    
    System.out.println("Enter second character to fill diamond: ");
    String character2 = myScanner.next();
    fillDiamond1 = character2.charAt(0);
    
    System.out.println("Enter third character to fill diamond: ");
    String character3 = myScanner.next();
    fillDiamond2 = character3.charAt(0);
    
    //Please be okay with that and give me full credit.
    
    //top half
    for (int i = 1; i <= sizeDiamond; i++) { //looping the top half of the program 
      for (int j = 1; j <=sizeDiamond; j++) {
        if (i >= sizeDiamond + 1 - j - (widthStripe/2) && i <= sizeDiamond + 1 - j + (widthStripe/2)) { //algorithm to determine the pattern
          System.out.print(fillStripe); //prints if it is correct
        } else if (i > j) { //if outer loop is greater than inner
          System.out.print(fillDiamond1);
        } else {
          System.out.print(fillDiamond2);
        }
        }
      
      for (int j = 1; j <=sizeDiamond; j++) {
        if (i >= j - (widthStripe/2) && i <= j + (widthStripe/2)) {
            System.out.print(fillStripe);
             }else if (sizeDiamond - j + 1 < i) {
                  System.out.print(fillDiamond1);
                  }else {
                    System.out.print(fillDiamond2);
                  }
       }
            System.out.println("");
    }
   
  //the bottom half of the program 
  for (int i = 1; i <= sizeDiamond; i++) {
      for (int j = 1; j <=sizeDiamond; j++) {
        if (i >= sizeDiamond + 1 - j - (widthStripe/2) && i <= sizeDiamond + 1 - j + (widthStripe/2)) {
          System.out.print(fillStripe);
        } else if (i > j) {
          System.out.print(fillDiamond1);
        } else {
          System.out.print(fillDiamond2);
        }
        }
      for (int j = 1; j <=sizeDiamond; j++) {
        if (i >= j - (widthStripe/2) && i <= j + (widthStripe/2)) {
          System.out.print(fillStripe);
        } else if (sizeDiamond - j + 1 > i) {
          System.out.print(fillDiamond1);
        } else {
          System.out.print(fillDiamond2);
        }
        }
      System.out.println("");
  }
 
  }
  
}