//Jordan Weintraub
//CSE2 lab04
//objective is to use if and switch statements to generate a card number and suit

import java.util.Scanner;

public class CardGenerator{ //declaring class
  public static void main(String[] args){ //declaring method
    int randomNumber = (int)(Math.random()*52)+1; //generating random number between 1 and 52
    String suit;
    String type;
    int number= randomNumber % 13; //divide random number by 13 and count remainder
   if ((randomNumber >=1) && (randomNumber <=13)){ //if statement to determine suit
      suit ="diamonds";
   } else if ((randomNumber >=14) && (randomNumber <=26)){ // else if statement determining next suit
      suit= "clubs";
   } else if ((randomNumber >=27) && (randomNumber <=39)){
      suit = "hearts";
   } else {//else to find last suit
      suit= "spades";
   } 

switch(number) { // switch to determine the match between remainder number and type of card numberc
  case 0:type = "Kings";
    break;
  case 1:type = " Ace ";
    break;
  case 2:type = " two ";
    break;
  case 3:type = " three ";
    break;
  case 4:type = "four";
    break;
  case 5:type = "five";
    break;
  case 6:type = "six";
    break;
  case 7:type = "seven";
    break;
  case 8:type = "eight";
    break;
  case 9:type = "nine";
    break;
  case 10:type = "ten";
    break;
  case 11:type = "Jack";
    break;
  case 12:type = "Queen";
    break;
  default: type = "Error"; //not sure if I need default statement since it must be between 1 and 13 but threw it in anyways.
    break;
}
    System.out.println( "You picked the " + type + " of " + suit); //print out type and suit of based on randomNumber
  }
}