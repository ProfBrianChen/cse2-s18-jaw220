//Jordan Weintraub
//CSE 2 lab08
//04/06/18
//Objective is to practice using arrays to get familiar with it being one-dimentional 

import java.util.Scanner;

public class Arrays{
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in);
    int NUM_Students = (int) Math.random() * 10 + 5; //generating random numbers between 5 and 10
    String[] students; //declaring array
    students = new String [NUM_Students]; //allocating space for array 
    int i = 0;
    for(i = 0; i < NUM_Students; i++){ //for loop to match name of student with student number 
    System.out.print("Enter name of student:");
      students[i] = myScanner.next();
    }
    
   
 
   int[] midterm; //declaring second array
   midterm = new int [NUM_Students]; //initializing array 
   for(i = 0; i < NUM_Students; i++){
     midterm[i] = (int) (Math.random() * 100) + 1; //generating random numbers between 1 and 100
   }
 
  for(i=0;i<NUM_Students;i++){
    System.out.println(students[i] + " " + midterm[i]); //printing out 2 arrays together 
  }
  }
}