//Jordan Weintraub
//CSE2 lab06
// Give me a free A please :)
// 03/09/18
//objective of this lab is to write while loops and use a break statement to hide the magic X

import java.util.Scanner;

public class encrypted_x { //declaring class
	public static void main(String[] args) { //declaring method
		Scanner myScanner = new Scanner(System.in);
		System.out.println( "Type in an integer between 0 and 100: ");
		int length; //declaring variable
		
		while(!myScanner.hasNextInt()) { //repeats loop until user enters right input
			System.out.print("Input a positive integer from 0 to 100:"); //re-printing statement so user can read it
			myScanner.next();//allowers user to move onto the next variable
		} 
		length = myScanner.nextInt(); //setting variable so it can be scanned
		while (length < 0 ){
	System.out.print("Input a positive integer from 0 to 100:");// printing out again the statement to ask user for input
	length = myScanner.nextInt();//move on to next scan
}
		for(int i = 0; i < length; i++){ //creates loop pattern that recognizes a space or a star
	for(int j = 0; j < length; j++){
		
		if(j==i||j+i==length-1) {
	System.out.print(" ");
}
		else{ 
			System.out.print("*");
		} 
	}

	System.out.println(); //prints everything at the end
}
		
	}
}
