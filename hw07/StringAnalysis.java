//Jordan Weintraub
//CSE2 hw07
//03/23/18
//objective is to analyze characters and determine if they are letters 

import java.util.Scanner;
public class StringAnalysis {
  public static void main (String[] args) {
    Scanner myScanner = new Scanner(System.in);
    int val = 0;
    
    System.out.println("Please enter a String: ");
    //takes in a new line
    String line = myScanner.nextLine();
    
    System.out.println("Would you like to examine all characters (Enter 1), or examine only some characters (Enter 2)");
    
    //while loop to check if value is an int and either 1 or 2
    while (val != 1 && val !=2) {
      System.out.println("Please enter a valid number: ");
      while (!myScanner.hasNextInt()) {
        System.out.println("Please enter a valid number: ");
        myScanner.next();
      }
      val = myScanner.nextInt();
    }
    
    //if value is 1 analyze entire string
    if (val == 1) {
      if (analyze(line)) {
        System.out.println("The string is all letters");
      }
      else {
        System.out.println("The string is not all letters");
      }
    }
    //if value is 2 analyze string up until character num
    else if (val == 2) {
      System.out.println("Please enter the number of characters to analyze: ");
      //while loop to take in number of characters
      while(!myScanner.hasNextInt()) {
        System.out.println("Please enter the number of characters to analyze: ");
        myScanner.next();
      }
      int num = myScanner.nextInt();
      if (analyze(line, num)) {
        System.out.println("The string is all letters");
      }
      else {
        System.out.println("The string is not all letters");
      }
    }
    
  }
  
  //method 1 to check entire string
  public static boolean analyze(String line) {
    //for loop goes through each character
    for (int i = 0; i < line.length(); i++) {
      if (Character.isLetter(line.charAt(i)) == false)
        return false;
    }
    return true;
  }
  
  //method 2 to check string up to character num
  public static boolean analyze(String line, int num) {
    //for loop goes through characters up until num
    for (int i = 0; i < num; i++) {
      if (Character.isLetter(line.charAt(i)) == false)
        return false;
    }
    return true;
  }
  
}