//Jordan Weintraub
//02/04/18
//CSE hw 02
//objective is to manipulating data stored in variables, in running simple calculations, and in printing the numerical output


public class Arithmetic {
   	public static void main(String[] args) {
       //Number of pairs of pants
       int numPants = 3;
       //Cost per pair of pants
       double pantsPrice = 34.98;

       //Number of sweatshirts
       int numShirts = 2;
       //Cost per shirt
       double shirtPrice = 24.99;

       //Number of belts
       int numBelts = 1;
       //cost per belt
       double beltPrice = 33.99;

       //the tax rate
       double paSalesTax = 0.06;

       //Calculated cost of each item before tax
       double totalCostOfPants = numPants * pantsPrice;
       double totalCostOfShirts = numShirts * shirtPrice;
       double totalCostOfBelts = numBelts * beltPrice;

       //Calculated cost of tax on each kind of item
       double TaxPants = paSalesTax * totalCostOfPants; 
       double TaxShirts = paSalesTax * totalCostOfShirts;
       double TaxBelts = paSalesTax * totalCostOfBelts;

       //Total cost of purchases before tax
       double TotalCostBeforeTax = totalCostOfPants + totalCostOfShirts + totalCostOfBelts;

       //Total sales tax
       double Tax = TotalCostBeforeTax * paSalesTax;
  
       //Total Paid for items
       double TotalCostAfterTax = Tax + TotalCostBeforeTax; 
      
      
       System.out.println("TotalCostBeforeTax = " + TotalCostBeforeTax);
       System.out.println("Tax = " + Math.round((Tax*100.0))/100.0);
       System.out.println("TotalCostAfterTax = " + Math.round((TotalCostAfterTax*100.0))/100.0);
      
    }
}

      
        
      