//Jordan Weintraub
//CSE2 lab05
//02/02/18
//objective is to a print a twist by creating a critical piece of syntax

import java.util.Scanner;

public class TwistGenerator { //declaring class
  public static void main(String[] args){ //declaring main method
    Scanner myScanner = new Scanner(System.in);
    int length = 0; 
    String junkWord;
    
    while (length <= 0){ //declaring loop
      System.out.println("Type in an integer known as length"); //asking user for length
      if (myScanner.hasNextInt() == true){ //runs loop if user types in integer
        length = myScanner.nextInt();
      }
      else {
        junkWord = myScanner.next(); //assigns false integer value to junkWord
      }
    }
    
   
    //runs loop with each character broken up 
    for (int i =0; i<length;i++) 
    {
      System.out.print('\\');
      i++;
      if (i>= length){ //sets breaking point if length is greater or equal to condition  
        break; //break statement to make sure it stops after every character before reading next condition
      }
      System.out.print(' ');
      i++;
      if (i>= length){
        break;
      }
      System.out.print('/');
      i++;
     }
    
    System.out.println();
    
    for (int i =0; i<length;i++) //same loop as above except this is for second line
    {
      System.out.print(' ');
      i++;
      if (i>= length){
        break;
      }
      System.out.print('X');
      i++;
      if (i>= length){
        break;
      }
      System.out.print(' ');
      i++;
    }
    System.out.println();
    
    for (int i =0; i<length;i++)
    {
      System.out.print('/');
      i++;
      if (i>= length){
        break;
      }
      System.out.print(' ');
      i++;
      if (i>= length){
        break;
      }
      System.out.print('\\');
      i++;
    }
    System.out.println();
    
  }
}