//Jordan Weintraub
//CSE2 lab07
//objective is to declare and call different methods 

import java.util.Random;
import java.util.Scanner;

public class Methods{ //declaring class
  public static void main(String[] args){ //declaring main methods
  Random randomGenerator = new Random() ;
  int randomInt = randomGenerator.nextInt(10); // randomly generating intergers less than 10
  System.out.print("The ");
  System.out.print(adjectives() + " "); //declaring first method while printing out adj
  System.out.print(subjects() + " ");
  System.out.print(verbs() + " ");
  System.out.print("the ");
  System.out.print(objects() + ".");
  
  Scanner myScanner = new Scanner(System.in);
  String subject1 = thesisGenerator();
  String action = actionGenerator();
  int pick;
    do {
      System.out.println("Enter any number for a pragraph, enter 0 to quit: ");
      int numberofsentences =randomGenerator.nextInt(10);
      pick = myScanner.nextInt();
        if (pick == 0) {
          return;
        }
      else {
        subject1 = thesisGenerator(randomGenerator);
        for (int i = 0; i < numberofsentences; i++) {
          action = actionGenerator(randomGenerator, subjects);
      }
      String conclusion = conclusionGenerator(randomGenerator, subjects);
    
    }
  } while (choice!=0);
  
}
  
  
    
public static String adjectives() { //first method used
Random randomGenerator = new Random(); //creating the random adjective
int randomInt = randomGenerator.nextInt(10); //this will generate random integers less than 10
String adjective = " ";
switch (randomInt) {
case 0:
adjective = "big";
break;
case 1:
adjective = "happy";
break;
case 2:
adjective = "curious";
break;
case 3: 
adjective = "funny";
break;
case 4: 
adjective = "creative";
break;
case 5: 
adjective = "nice";
break;
case 6: 
adjective = "smart";
break;
case 7:
adjective = "tall";
break;
case 8:
adjective = "grumpy";
break;
case 9:
adjective = "amazing";
break;

}
return adjective;
}

public static String subjects() { //next method
Random randomGenerator = new Random(); //creating the random subject 
int randomInt = randomGenerator.nextInt(10); //generates integers less than 10 to be used for switch statements
String subject = " ";
switch (randomInt) {
case 0: 
subject = "man";
break;
case 1: 
subject = "son";
break;
case 2: 
subject = "cat";
break;
case 3: 
subject = "grampa";
break;
case 4:
subject = "daughter";
break;
case 5:
subject = "woman";
break;
case 6: 
subject = "horse";
break;
case 7:
subject = "teacher";
break;
case 8: 
subject = "student";
break;
case 9:
subject = "dolphin";
break;
}
return subject; 
}

public static String verbs() { //repeated process for verbs
Random randomGenerator = new Random();
int randomInt = randomGenerator.nextInt(10); 
String verb = " ";
switch (randomInt) {
case 0:
verb = "jumped";
break;
case 1:
verb = "ran";
break;
case 2:
verb = "broke";
break;
case 3:
verb = "hit";
break;
case 4:
verb = "hitchhiked";
break;
case 5:
verb = "deleted";
break;
case 6:
verb = "consumed";
break;
case 7: 
verb = "beat";
break;
case 8:
verb = "provided";
break;
case 9:
verb = "attacked";
break;

}
return verb;
}
public static String objects() { //repeated process for objects 
Random randomGenerator = new Random();
int randomInt = randomGenerator.nextInt(10); 
String object = " ";
switch (randomInt) {
case 0:
object = "desk";
break;
case 1:
object = "chair";
break;
case 2:
object = "lamp";
break;
case 3:
object = "light";
break;
case 4:
object = "newspaper";
break;
case 5: 
object = "apple";
break;
case 6:
object = "grape";
break;
case 7:
object = "toy";
break;
case 8:
object = "outlet";
break;
case 9:
object = "doll";
break;

}
return object;
}
  
public static String thesisGenerator(Random randomGenerator) {
  Scanner myScanner = new Scanner(System.in);
  String adjective1 = adjectives(randomGenerator);
  String adjective2 = adjectives(randomGenerator);
  String adjective3 = adjectives(randomGenerator);
  String subject1 = subjects(randomGenerator);
  String action = verbs(randomGenerator);
  String object1 = objects(randomGenerator);
  System.out.println( "The " +adjective1+ " " +adjective2+ " " +subject1+ " " +action+ " the " +adjective3+ " " +object1+ " . ");
  return subject1;
}
 
public static String actionGenerator(Random randomGenerator, String subjects) {
  String object1 = object(randomGenerator);
  String object2 = object(randomGenerator);
  String object3 = object(randomGenerator);
  String action = verb(randomGenerator);
  String adjective1 = adjective(randomGenerator);
  String action1 = verb(randomGenerator);
  System.out.println( "The " +subject1+ " " +action1+ " " +object1+ " to  " +action+ " " +object2+ " at the " +adjective1+ " " +object3+ " . ");
  return adjective1;
  
  }
  
  
public static String conclusionGenerator(Random randomGenerator, String subjects) {
  String object1 = object(randomGenerator);
  String action = verb(randomGenerator);
  System.out.println("That" +subject1+ " " +action+ " her " +object1+ " . ");
  return action;
  }
  
  }
  
