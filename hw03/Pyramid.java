// Jordan Weintraub
// 02/11/18
// CSE hw03
// objective is to input data and practice performing arithmetic operations 

import java.util.Scanner;

public class Pyramid {
  public static void main(String []args) {
    
    Scanner myScanner = new Scanner (System.in); //constructing instacne
    System.out.print("The square side of the period is: "); //print out square side
    double Length = myScanner.nextDouble();
    System.out.print("The height of the period is: "); //print out length 
    double Height = myScanner.nextDouble();
    double LengthSquared = Math.pow(Length, 2); 
    double newVolume = (LengthSquared*Height)/3;
    System.out.printf("%#.2f\n", newVolume);
    
    
    
    
    
  }
}
