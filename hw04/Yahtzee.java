//Jordan Weintraub
//CSE2 hw04
//2/14/18
// objective is to re-create Yahtzee by practicing selection statements, operators, and String manipulation

import java.lang.Math;
import java.util.Scanner;
//import
public class Yahtzee { //defining class
  public static void main(String[] args) { //defining method
    int roll01 = 0; // initializing the dice
    int roll02 = 0;
    int roll03 = 0;
    int roll04 = 0;
    int roll05 = 0;
    
    Scanner input = new Scanner(System.in);
    System.out.println("Would you like to either roll randomly (Answer '1') or input a specific roll (Answer '2'):"); // printing option to pick random roll or enter numbers rolled
    int choice = input.nextInt();
    
    if (choice == 1){ // establishing random rolls for 5 dice
      int max = (int)Math.floor(6); // 6 is highest possible number per dice
      int min = (int)Math.ceil(1); // 1 is lowest possible number rolled per die
      roll01 = (int)(Math.random() * (max - min) +1) + min;
      roll02 = (int)(Math.random() * (max - min) +1) + min;
      roll03 = (int)(Math.random() * (max - min) +1) + min;
      roll04 = (int)(Math.random() * (max - min) +1) + min;
      roll05 = (int)(Math.random() * (max - min) +1) + min;
      
      System.out.println(roll01 +" "+ roll02 +" "+ roll03+ " "+ roll04+ " "+ roll05);
    }
    else if (choice == 2) { //input the digits rolled
      System.out.println("Input a 5 digit number representing 5 different dice rolls:");
      int rolls = input.nextInt();
      if (rolls >= 11111 && rolls <= 99999) { //barriers to make sure output is a 5 digit number
        roll01 = ((rolls/10000)%10); //modulus to find remainders
        roll02 = ((rolls/1000)%10);
        roll03 = ((rolls/100)%10);
        roll04 = ((rolls/10)%10);
        roll05 = (rolls%10);
        System.out.println(roll01 +" "+ roll02 +" "+ roll03+ " "+ roll04+ " "+ roll05);
      } 
      else {
        System.out.println("Please input 5 valid dice rolls");
      }
      
    }
    else {
      System.out.println("Error: Invalid Input (Please input '1' or '2')"); //making sure input follows guideilines 
      System.exit(1);
    }
    
    int Aces = 0; //declaring six variables that keep track of the values of each dice 
    int Twos = 0;
    int Threes = 0;
    int Fours = 0;
    int Fives = 0;
    int Sixes = 0;
    
    //finding out what number each die lands on
    switch(roll01) { 
      case 1:
        Aces++;
        break;
      case 2:
        Twos++;
        break;
      case 3:
        Threes++;
        break;
      case 4:
        Fours++;
        break;
      case 5: 
        Fives++;
        break;
      case 6:
        Sixes++;
        break;
    }
        
    switch(roll02) {
      case 1:
        Aces++;
        break;
      case 2:
        Twos++;
        break;
      case 3:
        Threes++;
        break;
      case 4:
        Fours++;
        break;
      case 5: 
        Fives++;
        break;
      case 6:
        Sixes++;
        break;
    }
        
    switch(roll03) {
      case 1:
        Aces++;
        break;
      case 2:
        Twos++;
        break;
      case 3:
        Threes++;
        break;
      case 4:
        Fours++;
        break;
      case 5: 
        Fives++;
        break;
      case 6:
        Sixes++;
        break;
    }
        
    switch(roll04) {
       case 1:
        Aces++;
        break;
      case 2:
        Twos++;
        break;
      case 3:
        Threes++;
        break;
      case 4:
        Fours++;
        break;
      case 5: 
        Fives++;
        break;
      case 6:
        Sixes++;
        break;
    }
    
    switch(roll05) {
      case 1:
        Aces++;
        break;
      case 2:
        Twos++;
        break;
      case 3:
        Threes++;
        break;
      case 4:
        Fours++;
        break;
      case 5: 
        Fives++;
        break;
      case 6:
        Sixes++;
        break;
    }
    
    int upperBoundScore = Aces + 2*Twos + 3*Threes + 4*Fours + 5*Fives + 6*Sixes; //summing total upper bound score 
    
    if (upperBoundScore >= 63) { //setting if statement to add bonus even tho it's impossible to achieve 
      upperBoundScore += 35;
    }
    
    int lowerScore=0;
 
    //Full house... note its mixed with 3 of a kind as an else statmenet because both have 3 of a kind in them so we want to avoid the overlap
    if (Aces==3 && (Twos==2 || Threes==2 || Fours==2||Fives==2||Sixes==2)) {
        lowerScore = 25;
       } else if (Twos==3 && (Aces==2 || Threes==2 || Fours==2||Fives==2||Sixes==2)) {
        lowerScore = 25; 
      } else if (Threes==3 && (Aces==2 || Twos==2 || Fours==2||Fives==2||Sixes==2)) {
        lowerScore = 25;
      } else if (Fours==3 && (Aces==2 || Twos==2 || Threes==2||Fives==2||Sixes==2)) {
        lowerScore = 25;
      } else if (Fives==3 && (Aces==2 || Twos==2 || Threes==2||Fours==2||Sixes==2)) {
        lowerScore = 25;
      } else if (Sixes==3 && (Twos==2 || Threes==2 || Fours==2||Fives==2||Aces==2)) {
        lowerScore = 25;
      } else 
    //Three of a Kind
        if (Aces==3) {
          lowerScore = Aces;
        }
        if (Twos==3) {
          lowerScore = 2*Twos; 
        }
        if (Threes==3) {
          lowerScore =3*Threes;
        }
        if (Fours==3) {
          lowerScore =4*Fours;
        }
        if (Fives==3) {
          lowerScore = 5*Fives;
        }
        if (Sixes==3) {
          lowerScore = 6*Sixes;
        }
      
          
    //Four of Kind
    if (Aces==4) {
       lowerScore += Aces;
     }
    if (Twos==4) {
      lowerScore += 2*Twos; 
    }
    if (Threes==4) {
      lowerScore +=3*Threes;
    }
    if (Fours==4) {
      lowerScore +=4*Fours;
    }
    if (Fives==4) {
      lowerScore += 5*Fives;
    }
    if (Sixes==4) {
      lowerScore += 6*Sixes;
    
    //Large Straight (MUST GO FIRST SO IT DOES NOT ADD SMALL STRAIGHT WITHIN LARGE!!!)
    } if (Aces>0 && Twos>0 && Threes>0 && Fours>0 && Fives >0) {
      lowerScore +=40;
    } else if (Sixes>0 && Twos>0 && Threes>0 && Fours>0 && Fives >0) {
      lowerScore +=40;
      
    //Small Straight
    } else if (Aces>0 && Twos>0 && Threes>0 && Fours>0) {
      lowerScore +=30;
    } else if (Fives>0 && Twos>0 && Threes>0 && Fours>0) {
      lowerScore +=30;
    } else if (Fives>0 && Sixes>0 && Threes>0 && Fours>0) {
      lowerScore +=30;
    }
    //Yahtzee
     if (Aces==5 || Twos==5 ||Threes==5 || Fours==5 || Fives==5 || Sixes==5) {
      lowerScore +=50;
    }
    //chance 
    if (lowerScore == 0){
      lowerScore = Aces + 2*Twos + 3*Threes + 4*Fours + 5*Fives + 6*Sixes;
    }
    
    System.out.println( "Your total Upper score is " + upperBoundScore ); //printing score of upper bound
    
    System.out.println( "Your total Lower score is " + lowerScore ); //printing score of lower bound
    int grandTotal = upperBoundScore + lowerScore;
    System.out.println( "Your Grand total score is " + grandTotal); //total score per game
    
  }
}

  