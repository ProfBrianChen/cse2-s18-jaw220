//Jordan Weintraub
//CSE2 HW09 
//objective is to manipulate arrays and in writing methods that have array parameters.
//more specifically, it is to draw a random hand in poker and compare who wins 
import java.util.Random;
import java.lang.Math;
import java.util.Scanner;

public class DrawPoker{
public static void main(String[] args){
  int[] deck; //declared array 
  deck = new int[52];
  for(int i = 0; i < 52; i++){ //filling up array with values
    deck[i] = i + 1;
  }
  //shuffle deck
   Random random = new Random();
  for (int i = 0; i < deck.length; i++){
    int r = i + random.nextInt(deck.length - i); //randomly generating index for swapping
    int temp = deck[r];
    deck[r] = deck[i];
    deck[i] = temp; //swaps indexes 
   }
  
  int[] player1 = new int[5]; //initializing each palyer's hand
  int[] player2 = new int[5];
  int j = 0;
  for(int i = 0; i < 5; ++i){ //setting values from the deck to player's hands 
    player1[i] = deck[j];
    player2[i] = deck[j+1];
    j+=2;
  }

   System.out.print("Player 1: ");
   for(int i =0; i<5;i++){
     System.out.print(player1[i] % 13+ " ");
   }
   System.out.println();
   System.out.print("Player 2: ");
  for(int i =0; i<5;i++){
     System.out.print(player2[i] % 13+ " ");
   }
  System.out.println();
  //setting all the precendents to determine who wins each draw
  if (fullhouse(player1) && !fullhouse(player2)){
    System.out.println("Player 1 wins with a Full House.");
  }
  else if (fullhouse(player2) && !fullhouse(player1)){
    System.out.println("Player 2 wins with a Full House.");
  }
  else if (fullhouse(player1) && fullhouse(player2)){
    System.out.println("Players tie");
  }
  else if (flush(player1) && !flush(player2)){
    System.out.println("Player 1 wins with a flush");
  }
  else if (flush(player2) && !flush(player1)){
    System.out.println("Player 2 wins with a flush"); 
  }
  else if (flush(player1) && flush(player2)){
    System.out.println("Players tie");
  }
  else if (threeOfKind(player1) > 0 && threeOfKind(player2) == 0){
    System.out.println("Player 1 wins with a 3 of a kind"); 
  }
  else if (threeOfKind(player2) > 0 && threeOfKind(player1) == 0){
    System.out.println("Player 2 wins with a 3 of a kind"); 
  }
  else if (threeOfKind(player1) > 0 && threeOfKind(player2) > 0){
    System.out.println("Players tie");
  }
  else if (pair(player1) > 0 && pair(player2) == 0){
    System.out.println("Player 1 wins with a pair"); 
  }
  else if (pair(player2) > 0 && pair(player1) == 0){
    System.out.println("Player 2 wins with a pair"); 
  }
  else if (pair(player1) > 0 && pair(player2) > 0){
    System.out.println("Players tie");
  }
  else if (highCard(player1) > highCard(player2)){
    System.out.println("Player 1 wins with a high card"); 
  }
  else if (highCard(player2) > highCard(player1)){
    System.out.println("Player 2 wins with a high card"); 
  }
  else if (highCard(player1) == highCard(player2)){
    System.out.println("Players tie");
  }
  
  
}
  public static int pair(int[] array){
   //nested forloop comparing each value for pairs 
    for(int i = 0; i< array.length; i++){ 
      int value = array[i] % 13;
      for(int j = i+1; j < array.length; j++){ 
        if (value == array[j] % 13){
          return value; // returned number is the index value of pair
        }
      }
    }
    return 0; // 0 == false
  }
  public static int threeOfKind(int[] array){
    int value = pair(array); //value of pair 
    if (value > 0){ //if the number value is a pair
      for (int j = 0; j< array.length; ++j){
        int value1 = array[j] % 13;
        int flag = 1; //resetting flag back to 1 so it doesn't continue to add on 
        for (int i = j+1; i< array.length; i++){ //will auto-find 2 matched values but looking for 3rd
          if (value1 == (array[i] % 13)){ 
            ++flag; //counts number of same numbered cards
            if(flag == 3){
              return value1;
            }
          }
        }
      }
    }
    return 0;
   
  }
  
  public static boolean flush(int[]array){
    int flag = 0;
    int value = array[0] / 13;
    for(int j = 1; j < array.length; j++){
        if (value == array[j] /13){
            ++flag; //flag the number every time it has the same value 
        }
    }
    if (flag == 4){ //will return true if all cards are same suit 
        return true;
    }
      
    return false;
  }
  
  
 public static boolean fullhouse(int[]array){
  if ((pair(array) > 0) && (threeOfKind(array) > 0) && (pair(array) != threeOfKind(array))){
    return true;
  }
    return false;
 }

 public static int highCard(int[] array){
   int max = array[0];
   for( int i = 0;i< array.length; i++){
     if (max < array[i]){
       max = array[i];
     }
   }
   return max;
 }

}
  
